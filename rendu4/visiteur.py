import psycopg2

try:
    conn = psycopg2.connect("dbname='dbnf18a033' user='nf18a033' password='7OzX1eycMOQL' host='tuxa.sme.utc'")
except psycopg2.OperationalError as e:
    print("Message personnalisé : contrainte non respectée")
    print("Message système :", e)
    conn.rollback()


def affichageTableVisiteur():
    """Affiche les noms des tables qu'un visiteur peut consulter"""
    choix = 4
    while choix != 0:
        print("Que voulez vous consulter ?\n")
        print("1. Les expositions en cours.\n"
              "2. Les salles dédiées à une exposition temporaire recherchée.\n"
              "3. Connaître les informations des œuvres.\n"
              "0. Quitter\n")
        choix = int(input("Entrez votre choix : "))
        if choix == 1:
            print("Voici les expositions en cours :\n")
        elif choix == 2:
            print("De quelle exposition voulez-vous connaître les salles ?\n")

            print("Voici les salles dédiée à l'exposition :\n")
        elif choix == 3:
            print("Voici toutes les informations sur les oeuvres : \n ")
        elif choix == 0:
            print("Merci pour votre visite. À bientôt !\n")
        else:
            print("Choix impossible, veuillez choisir un numéro parmi ceux proposés.\n")


if __name__ == '__main__':
    affichageTableVisiteur()
# conn.close()
