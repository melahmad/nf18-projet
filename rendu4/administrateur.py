import psycopg2

try:
    conn = psycopg2.connect("dbname='dbnf18a033' user='nf18a033' password='7OzX1eycMOQL' host='tuxa.sme.utc'")
except psycopg2.OperationalError as e:
    print("Message personnalisé : contrainte non respectée")
    print("Message système :", e)
    conn.rollback()

noms_tables_admin = ["Auteur", "Musee", "Oeuvre", "Cree", "Restauration", "Peinture", "Sculpture", "Photo",
                     "Exposition", "Presente", "Exposition_Temporaire", "Exposition_permanente", "Salle",
                     "Panneau_Explicatif", "Creneau", "Guide", "Affecte", "Mene_exp_temp", "Mene_exp_perm"]

cur = conn.cursor()


def ajoutDonneeAdmin(conn, cur):
    """Ajoute des données à une table"""
    print("À quel table voulez-vous ajouter des données ?")
    for i in range(len(noms_tables_admin)):
        print(f'{i}.{noms_tables_admin[i]}\n')
    choix_table = int(input("Saisir votre choix : "))

    conn.commit()


def suppressionDonneesAdmin(conn, cur):
    """Supprime des données à une table"""
    print("À quelle table voulez-vous supprimer des données ?")
    for i in range(len(noms_tables_admin)):
        print(f'{i}.{noms_tables_admin[i]}\n')
    choix_table = int(input("Saisir votre choix : "))
    conn.commit()


def affichageTableAdmin(cur):
    """Affiche les noms des tables qu'un administrateur peut consulter et affiche son contenu"""
    print("Quel table voulez vous consulter ?")
    for i in range(len(noms_tables_admin)):
        print(f'{i}.{noms_tables_admin[i]}\n')
    choix_table = int(input("Saisir votre choix : "))
    sql = ""
    cur.execute(sql)

def modificationDonneeAdmin(cur):
    print("Quel table voulez vous modifier ?")
    for i in range(len(noms_tables_admin)):
        print(f'{i}.{noms_tables_admin[i]}\n')
    choix_table = int(input("Saisir votre choix: "))

def creationTableAdmin(conn, cur):
    """Crée une nouvelle table"""
    sql = input("Entrez la requête pour créer une nouvelle table : ")
    conn.commit()


def suppressionTableAdmin(conn, cur):
    """Supprimer une table existante"""
    print("Quelle table voulez-vous supprimer")
    for i in range(len(noms_tables_admin)):
        print(f'{i}.{noms_tables_admin[i]}\n')
    choix_table = int(input("Saisir votre choix : "))
    conn.commit()


conn.close()
