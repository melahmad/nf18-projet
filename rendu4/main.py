import psycopg2

import administrateur
import conservateur_musee
import guide
import restaurateur
import visiteur


try:
    conn = psycopg2.connect("dbname='dbnf18a033' user='nf18a033' password='7OzX1eycMOQL' host='tuxa.sme.utc'")
except psycopg2.OperationalError as e:
    print("Message personnalisé : contrainte non respectée")
    print("Message système :", e)
    conn.rollback()

cur = conn.cursor()

def Administrateur():
    ans = True
    print("Menu Administrateur\n--------------------\n")
    while ans:
        print("""
        1. Ajouter des données à une table existante.
        2. Supprimer des données d'une table existante.
        3. Afficher des données d'une table existante.
        4. Modifier des données d'une table existante.
        5. Créer une nouvelle table.
        6. Supprimer une table.
        0. Exit/Quit
        """)
        ans = input("Entrez votre choix :\n")
        if ans == "1":
            administrateur.ajoutDonneeAdmin()
        elif ans == "2":
            administrateur.suppressionDonneesAdmin()
        elif ans == "3":
            administrateur.affichageTableAdmin()
        elif ans == "4":
            administrateur.m
        elif ans == "5":
            print("")
            # appel de la fonction
        elif ans == "6":
            print("")
            # appel de la fonction
        elif ans == "0":
            # appeler la fonction
            print("\n Fin du programme")
            raise SystemExit
        else:
            print("\n Ce choix n'existe pas, veuillez choisir un entier parmi les choix proposés.\n")


def Visiteur():
    print("Menu Visiteur\n--------------------\n")
    ans = True
    while ans != 0:
        print("""
        1.Consulter des données
        0. Exit/Quit
        """)
        ans = input("Que voulez-vous faire ?")
        if ans == "1":
            visiteur.affichageTableVisiteur()

        elif ans == "0":
            print("\n Merci. Fin du programme")
            raise SystemExit
        else:
            print("\n Ce choix n'existe pas, veuillez choisir un entier parmi les choix proposés.\n")


def Restaurateur():
    print("Menu Restaurateur\n--------------------\n")
    ans = True
    while ans != 0:
        print("""
        1. Consulter des données
        0. Exit/Quit
        """)
        ans = input("Que voulez-vous faire ?")
        if ans == "1":
            restaurateur.affichageTableRestaurateur()

        elif ans == "0":
            print("\n Merci. Fin du programme")
            raise SystemExit
        else:
            print("\n Ce choix n'existe pas, veuillez choisir un entier parmi les choix proposés.\n")


def Guide():
    print("Menu Guide\n--------------------\n")
    ans = True
    while ans != 0:
        print("""
        1. Consulter des données
        0. Exit/Quit
        """)
        ans = input("Que voulez-vous faire ?")
        if ans == "1":
           guide.affichageTableGuide()

        elif ans == "0":
            print("\n Fin du programme")
            raise SystemExit
        else:
            print("\n Ce choix n'existe pas")


def Conservateur_musee():
    print("Menu Conservateur de musée\n--------------------\n")
    ans = True
    while ans != 0:
        print("""
            1. Consulter des données.
            2. Consulter des statistiques.
            0. Exit/Quit
            """)
        ans = input("Que voulez-vous faire ?")
        if ans == "1":
            conservateur_musee.consultationDonnees()
        elif ans == "2":
            conservateur_musee.consultationStatistique()
        elif ans == "0":
            print("\n Fin du programme")
            raise SystemExit
        else:
            print("\n Ce choix n'existe pas")


def menu():
    print("Bienvenue dans la base de données du musée du Louvre\n----------------------------------------------------\n")

    print("\nQuel est votre rôle au sein de la base de données ?\n")

    print("""
        1. Administrateur
        2. Conservateur de musée
        3. Visiteur
        4. Restaurateur
        5. Guide
        0. Exit/Quit\n
        """)

    choix = input()
    if choix == "1":
        Administrateur()
    elif choix == "2":
        Conservateur_musee()
    elif choix == "3":
        Visiteur()
    elif choix == "4":
        Restaurateur()
    elif choix == "5":
        Guide()
    elif choix == "0":
        print("Au revoir\n")
    else:
        print("\n Ce choix n'existe pas")


if __name__ == '__main__':
    menu()
