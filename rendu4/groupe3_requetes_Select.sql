/*
Connaître  le titre, la date, l’auteur (avec ses dates) et les dimensions
d’une oeuvre donnée (l’id);
*/

SELECT titre, date, dimension, Nom, date_naiss, date_mort
FROM Oeuvre
JOIN Cree ON Oeuvre.ID_Oeuvre = Cree.ID_Oeuvre
JOIN Auteur ON Auteur.ID_Nom = Cree.ID_Auteur
WHERE Oeuvre.ID_Oeuvre = id;

/*
Exemple pour l'id 5 :
SELECT titre, date, dimension, Nom, date_naiss, date_mort
FROM Oeuvre
JOIN Cree ON Oeuvre.ID_Oeuvre = Cree.ID_Oeuvre
JOIN Auteur ON Auteur.ID_Nom = Cree.ID_Auteur
WHERE Oeuvre.ID_Oeuvre = 5;
*/

/*
Savoir le nom et l’adresse, d’un musée extérieur dont les oeuvres ont été
empruntées par Louvre pour une exposition temporaire donnée (nom) ;
*/

SELECT Musee.nom, Musee.adresse
FROM Musee
JOIN Oeuvre ON Musee.id_musee = Oeuvre.id_musee
JOIN Presente ON Oeuvre.id_oeuvre = Presente.id_oeuvre
JOIN Exposition_Temporaire ON Presente.nom_exposition = Exposition_Temporaire.nom
WHERE Oeuvre.deja_emprunte = TRUE
AND Exposition_Temporaire.nom= 'nom donné';
/*
Exemple pour le nom d'exposition temporaire 'Naples à Paris' :
SELECT Musee.nom, Musee.adresse
FROM Musee
JOIN Oeuvre ON Musee.id_musee = Oeuvre.id_musee
JOIN Presente ON Oeuvre.id_oeuvre = Presente.id_oeuvre
JOIN Exposition_Temporaire ON Presente.nom_exposition = Exposition_Temporaire.nom
WHERE Oeuvre.deja_emprunte = TRUE
AND Exposition_Temporaire.nom= 'Naples à Paris';
*/

/*
Savoir le nom et l'adresse, de tous les musées extérieurs dont les oeuvres ont
été empruntées par Louvre ;
*/

SELECT Musee.nom, Musee.adresse
FROM Musee
JOIN Oeuvre ON Musee.id_musee = Oeuvre.id_musee
JOIN Presente ON Oeuvre.id_oeuvre = Presente.id_oeuvre
JOIN Exposition_Temporaire ON Presente.nom_exposition = Exposition_Temporaire.nom
WHERE Oeuvre.deja_emprunte = TRUE;

/*
Savoir la  date de début et la date de fin d’un emprunt d’une oeuvre ;
*/

SELECT titre, date_debut_emprunt, date_fin_emprunt
FROM Oeuvre
WHERE Oeuvre.deja_emprunte = TRUE;

/*
Savoir le prix d'acquisition des œuvres appartenant au Louvre.
*/
SELECT prix_acquisition
FROM Oeuvre
JOIN Musee ON Oeuvre.id_musee = Musee.id_musee
WHERE Musee.nom = 'Musee du Louvre';

/*
Savoir le type de restauration, la raison sociale du prestataire ainsi que la
date et le montant de la prestation, selon le nom d’une oeuvre donné ;
*/

SELECT Restauration.type, Restauration.raison_sociale, Restauration.date, Restauration.montant
FROM Restauration
JOIN Oeuvre ON Restauration.id_oeuvre = Oeuvre.id_oeuvre
WHERE Oeuvre.titre ='nom de l’oeuvre';

/* Exemple pour l'oeuvre Mystérieuse Antea :
SELECT Restauration.type, Restauration.raison_sociale, Restauration.date, Restauration.montant
FROM Restauration
JOIN Oeuvre ON Restauration.id_oeuvre = Oeuvre.id_oeuvre
WHERE Oeuvre.titre ='Mystérieuse Antea';

/*
Savoir les salles dédiées à l'exposition (nom de l’exposition) ;
*/
SELECT numero
FROM SALLE
WHERE exposition = 'nom exposition';

/*
Exemple pour l'exposition Naples à Paris
SELECT numero
FROM SALLE
WHERE exposition = 'Naples à Paris';
*/

/*
Savoir si l’oeuvre(id)  a déjà été prêtée ou non ; Mettre avec le nom de l'oeuvre ?
*/
SELECT deja_prete
FROM Oeuvre
WHERE Oeuvre.ID_Oeuvre = id;

/*
Exemple avec l'id 3
SELECT deja_prete
FROM Oeuvre
WHERE Oeuvre.ID_Oeuvre = 3;
*/

/*
Savoir si l’oeuvre(id)  a déjà été empruntée ou non ;
*/
SELECT deja_emprunte
FROM Oeuvre
WHERE Oeuvre.ID_Oeuvre = id;
/*
Exemple pour l'id 4
SELECT deja_emprunte
FROM Oeuvre
WHERE Oeuvre.ID_Oeuvre = 4;
*/

/*
Savoir qui mène une exposition permanente selon son nom;
*/
SELECT Guide.nom AS nom_guide, Guide.prenom AS prenom_guide, date_embauche
FROM Mene_exp_perm
JOIN Guide ON Guide.id = Mene_exp_perm.id
JOIN Exposition_permanente ON Exposition_permanente.nom = Mene_exp_perm.nom
WHERE Exposition_permanente.nom = 'nom_donné';
/*
Exemple pour l'exposition Cubisme
SELECT Guide.nom AS nom_guide, Guide.prenom AS prenom_guide, date_embauche
FROM Mene_exp_perm
JOIN Guide ON Guide.id = Mene_exp_perm.id
JOIN Exposition_permanente ON Exposition_permanente.nom = Mene_exp_perm.nom
WHERE Exposition_permanente.nom = 'Cubisme';
*/

/*
Savoir à quel créneau est affecté un guide selon son nom et prénom ;
*/
SELECT jour, h_debut
FROM Affecte
JOIN Guide ON Affecte.guide = Guide.id
WHERE Guide.nom = 'nom du guide' AND Guide.prenom = 'prenom du guide';
/*
Exemple pour le guide Garnier	Lucie
SELECT jour, h_debut
FROM Affecte
JOIN Guide ON Affecte.guide = Guide.id
WHERE Guide.nom = 'Garnier' AND Guide.prenom = 'Lucie';
*/

/*
Savoir le prix moyen de restauration de type et raison sociale donnés ;
*/
SELECT AVG(montant) AS prix_moyen
FROM Restauration
WHERE raison_sociale = 'raison_sociale_choisi'  AND type='type_choisi';

/*
Exemple pour la raison sociale : Bérengère BALLET-GOULARD et le type Nettoyage
SELECT AVG(montant) AS prix_moyen
FROM Restauration
WHERE raison_sociale = 'Bérengère BALLET-GOULARD'  AND type='Nettoyage';
*/

/*
Toutes les raisons sociales du plus chères à la moins chère (en regardant le
prix moyen de restauration) ;
*/
SELECT raison_sociale,
AVG(montant) AS prix_moyen_restauration
FROM Restauration
GROUP BY raison_sociale
ORDER BY prix_moyen_restauration DESC;


/*
Savoir combien il y a de restauration de chaque type ;
*/
SELECT type, COUNT(ID_Restauration) AS nombre_de_restauration
FROM Restauration
GROUP BY type;

/*
Savoir toutes les restaurations d’une oeuvre donnée (l’id)
*/
SELECT *
FROM Restauration
WHERE ID_oeuvre = id;
/*
Exemple pour l'id 3:
SELECT *
FROM Restauration
WHERE ID_oeuvre = 3;*/

/*
Savoir les creneaux des expositions
/*

/*
Savoir quelles sont les expositions actuellement en cours
*/
SELECT Exposition.nom
FROM Exposition
LEFT JOIN Exposition_permanente ON Exposition_permanente.nom = Exposition.nom
LEFT JOIN Exposition_temporaire ON Exposition_temporaire.nom = Exposition.nom
WHERE
 Exposition_permanente.nom IS NOT NULL
 OR
( Exposition_temporaire.date_debut <= NOW()
 AND NOW() <= Exposition_temporaire.date_fin);

/*
Le prix moyen d'acquisition des œuvres des expositions;
*/
SELECT p.nom_exposition, AVG(o.prix_acquisition) AS prix_moyen_acquisition
FROM Oeuvre o
JOIN Presente p ON o.ID_oeuvre = p.ID_oeuvre
GROUP BY p.nom_exposition;

/*
Coût total d’acquisition par musées
*/
SELECT  m.nom AS nom_musee, SUM(o.prix_acquisition) AS cout_total_acquisition
FROM Oeuvre o
JOIN Musee m ON o.ID_Musee = m.ID_Musee
GROUP BY m.nom;

/*
Max, min et moyenne durée de l'exposition temporaire
*/
SELECT
MAX(DATEDIFF(day, date_debut, date_fin)) AS duree_max,
MIN(DATEDIFF(day, date_debut, date_fin)) AS duree_min,
AVG(DATEDIFF(day, date_debut, date_fin)) AS duree_moyenne
FROM Exposition_Temporaire;

/*
Vue prestataire
*/
CREATE VIEW prestataire AS
SELECT raison_sociale
FROM Restauration
GROUP BY raison_sociale;
