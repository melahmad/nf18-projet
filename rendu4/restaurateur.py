import psycopg2

try:
    conn = psycopg2.connect("dbname='dbnf18a033' user='nf18a033' password='7OzX1eycMOQL' host='tuxa.sme.utc'")
except psycopg2.OperationalError as e:
    print("Message personnalisé : contrainte non respectée")
    print("Message système :", e)
    conn.rollback()


def affichageTableRestaurateur(conn):
    """Affiche les noms des tables qu'un restaurateur peut consulter"""
    choix = 4
    raison_sociale = input("Quel est votre raison sociale ?")

    while choix != 0:
        print("Que voulez-vous consulter ?\n")
        print("1. Savoir vos restaurations en cours.\n"
              "2. Savoir l'historique des restaurations d'une œuvre.\n"
              "0. Quitter\n")
        choix = int(input("Entrez votre choix : "))
        if choix == 1:
            print("Voici vos restaurations en cours :\n")
        elif choix == 2:
            oeuvre=input("De quelle œuvre voulez-vous connaître l'historique de restauration ?")
            print(f"Voici l'historique des restaurations de l'œuvre : {oeuvre} :\n")
        elif choix == 0:
            print("Merci pour votre visite. À bientôt !\n")
        else:
            print("Choix impossible, veuillez choisir un numéro parmi ceux proposés.\n")


conn.close()
