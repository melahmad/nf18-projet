import psycopg2

try:
    conn = psycopg2.connect("dbname='dbnf18a033' user='nf18a033' password='7OzX1eycMOQL' host='tuxa.sme.utc'")
except psycopg2.OperationalError as e:
    print("Message personnalisé : contrainte non respectée")
    print("Message système :", e)
    conn.rollback()


def affichageTableGuide():
    """Affiche les noms des tables qu'un guide peut consulter"""
    choix = 4
    nom = input("Quel est votre nom ?")
    prenom = input("Quel est votre prénom ?")

    while choix != 0:
        print("Que voulez-vous consulter ?\n")
        print("1. Savoir si vous faites partie de ceux qui mènent une exposition permanente.\n"
              "2. Savoir à quel créneau vous devez travailler.\n"
              "0. Quitter\n")
        choix = int(input("Entrez votre choix : "))
        if choix == 1:
            print("Voici les guides pour les  expositions permanentes :\n")
        elif choix == 2:
            print("Voici vos créneaux :\n")
        elif choix == 0:
            print("Merci pour votre visite. À bientôt !\n")
        else:
            print("Choix impossible, veuillez choisir un numéro parmi ceux proposés.\n")


conn.close()