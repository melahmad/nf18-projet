import psycopg2

try:
    conn = psycopg2.connect("dbname='dbnf18a033' user='nf18a033' password='7OzX1eycMOQL' host='tuxa.sme.utc'")
except psycopg2.OperationalError as e:
    print("Message personnalisé : contrainte non respectée")
    print("Message système :", e)
    conn.rollback()

cur = conn.cursor()


def affichageOeuvre():
    """Affiche toutes les œuvres"""
    sql = "SELECT ID_oeuvre, titre FROM Oeuvre;"
    cur.execute(sql)
    res = cur.fetchall()
    print("--------------------ŒUVRES--------------------")
    idOeuvres = []
    for raw in res:
        print(f"{raw[0]}- {raw[1]}\n")
        idOeuvres.append(raw[0])
    choix_oeuvre = 0
    while choix_oeuvre not in idOeuvres:
        try:
            choix_oeuvre = int(input("Quelle œuvre voulez-vous consulter? Entrez l'ID de l'œuvre : "))
            if choix_oeuvre not in idOeuvres:
                print("Veuillez sélectionner un ID d'œuvre valide.\n")
        except ValueError:
            print("Veuillez entrer un nombre entier.\n")
    return choix_oeuvre


def affichageRaisonSociale():
    """Affiche toutes les raisons sociales"""
    sql = "SELECT DISTINCT raison_sociale FROM Restauration;"
    cur.execute(sql)
    res = cur.fetchall()
    print("------------------RAISON SOCIALE------------------\n")
    i = 1
    raisonSociale = []
    for raw in res:
        print(f"{i}- {raw[0]}\n")
        raisonSociale.append(raw[0])
        i += 1
    choix_RS = ""
    while choix_RS not in raisonSociale:
        try:
            choix_idRS = int(input("Quel prestataire voulez-vous consulter? Entrez le numéro de la raison sociale : "))
            if 1 <= choix_idRS <= len(raisonSociale):
                choix_RS = raisonSociale[choix_idRS - 1]
            else:
                print("Veuillez entrer un numéro valide.\n")
        except ValueError:
            print("Veuillez entrer un nombre entier.\n")
    return choix_RS


def affichageType():
    """Affiche tous les types de restauration"""
    sql = "SELECT DISTINCT type FROM Restauration;"
    cur.execute(sql)
    res = cur.fetchall()
    print("------------------TYPE DE RESTAURATION------------------\n")
    i = 1
    typeR = []
    for raw in res:
        print(f"{i}- {raw[0]}\n")
        typeR.append(raw[0])
        i += 1
    choix_type = ""
    while choix_type not in typeR:
        try:
            choix_idT = int(input("Quelle type voulez vous consulter? Entrez le numéro du type de restauration : "))
            if 1 <= choix_idT <= len(typeR):
                choix_type = typeR[choix_idT - 1]
            else:
                print("Veuillez entrer un numéro valide.\n")
        except ValueError:
            print("Veuillez entrer un nombre entier.\n")
    return choix_type


def affichageExpo():
    """Affiche toutes les expositions"""
    sql = "SELECT nom FROM Exposition;"
    cur.execute(sql)
    res = cur.fetchall()
    print("------------------EXPOSITION-----------------\n")
    i = 1
    expositions = []
    for raw in res:
        print(f"{i}- {raw[0]}\n")
        expositions.append(raw[0])
        i += 1
    choix_expo = ""
    while choix_expo not in expositions:
        try:
            choix_idE = int(input("Quelle exposition voulez vous consulter? Entrez le numéro de l'exposition : "))
            if 1 <= choix_idE <= len(expositions):
                choix_expo = expositions[choix_idE - 1]
            else:
                print("Veuillez entrer un numéro valide.\n")
        except ValueError:
            print("Veuillez entrer un nombre entier.\n")
    return choix_expo


def consultationDonnees():
    """Affiche les données d'une table donnée"""
    choix = -1
    while choix != 0:
        print("Quel type de données voulez-vous consulter ?\n")
        print("1- Les expositions actuellement en cours.\n")
        print("2- Les restaurations d’une oeuvre donnée.\n")
        print("3- Le nombre de restauration de chaque type.\n")
        print("4- Les raisons sociales de la plus chère à la moins chère selon le prix moyen de restauration.\n")
        print("5- Le prix moyen de restauration de type et raison sociale donnés.\n")
        print("6- L'emprunt ou non d'une œuvre.\n")
        print("7- Le prêt ou non d'une œuvre.\n")
        print("8- Les salles dédiées à une exposition.\n")
        print("9- Toutes les informations d'une prestation de restauration d'une œuvre.\n")
        print("10- Le prix d'acquisition des œuvres appartenant au Louvre.\n")
        print("11- La date de début et la date de fin d’un emprunt d’une oeuvre.\n")
        print("12- Les informations de tous les musées extérieurs dont les œuvres ont été empruntées par le Louvre.\n")
        print("13- Les informations d’un musée extérieur dont les œuvres ont été empruntées par le Louvre pour une "
              "exposition temporaire.\n")
        print("14- Les informations d’une œuvre.\n")
        print("0- Quitter.\n")
        choix = int(input("Quel est votre choix ?\n"))
        print("Voici les données que vous vouliez consulter : \n")

        if choix == 1:
            sql = "SELECT Exposition.nom FROM Exposition LEFT JOIN Exposition_permanente ON Exposition_permanente.nom = Exposition.nom LEFT JOIN Exposition_temporaire ON Exposition_temporaire.nom = Exposition.nom WHERE Exposition_permanente.nom IS NOT NULL OR ( Exposition_temporaire.date_debut <= NOW() AND NOW() <= Exposition_temporaire.date_fin);"
            cur.execute(sql)
            res = cur.fetchall()
            print("---------------EXPOSITIONS ACTUELLEMENT EN COURS--------------")
            for raw in res:
                print(f"Exposition : {raw[0]}\n")

        elif choix == 2:
            id_oeuvre = int(affichageOeuvre())
            sql = f"SELECT * FROM Restauration WHERE ID_oeuvre = {id_oeuvre};"
            cur.execute(sql)
            res = cur.fetchall()
            print("--------------RESTAURATION DE L'ŒUVRE--------------\n")
            for raw in res:
                print(
                    f"- id restauration {raw[0]} | date {raw[1]} | montant {raw[2]}€ | type {raw[3]} | id Oeuvre {raw[4]} | Raison Sociale {raw[5]}\n")

        elif choix == 3:
            sql = "SELECT type, COUNT(ID_Restauration) AS nombre_de_restauration FROM Restauration GROUP BY type;"
            cur.execute(sql)
            res = cur.fetchall()
            print("---------------NOMBRE DE RESTAURATION PAR TYPE DE RESTAURATION----------------\n")
            for raw in res:
                print(f"Type : {raw[0]}  | Nombre de restauration : {raw[1]} \n")
                print("--------------------------------------------------------")

        elif choix == 4:
            sql = "SELECT raison_sociale, AVG(montant) AS prix_moyen_restauration FROM Restauration GROUP BY raison_sociale ORDER BY prix_moyen_restauration DESC;"
            cur.execute(sql)
            res = cur.fetchall()
            print("---------------PRESTATAIRE DU PLUS AU MOINS CHÈRE---------------\n")
            for raw in res:
                print(f"Raison_sociale {raw[0]} | Prix moyen de restauration {raw[1]}€\n")
                print("---------------------------------------------------------------------------")

        elif choix == 5:
            raison_sociale_choisi = affichageRaisonSociale()
            type_choisi = affichageType()
            sql = f"SELECT AVG(montant) AS prix_moyen FROM Restauration WHERE raison_sociale = '{raison_sociale_choisi}' AND type='{type_choisi}';"
            cur.execute(sql)
            res = cur.fetchall()
            print("--------------PRIX MOYEN RESTAURATION--------------\n")
            for raw in res:
                print(f"Prix moyen : {raw[0]}€")

        elif choix == 6:
            id = int(affichageOeuvre())
            sql = f"SELECT deja_emprunte FROM Oeuvre WHERE Oeuvre.ID_Oeuvre = {id};"
            cur.execute(sql)
            res = cur.fetchall()
            print("------------EMPRUNT-------------\n")
            for raw in res:
                if raw[0] == 0:
                    print("L'œuvre n'a jamais été empruntée.\n")
                elif raw[0] == 1:
                    print("L'œuvre a déjà été empruntée.\n")
                else:
                    print("Erreur.\n")

        elif choix == 7:
            id = int(affichageOeuvre())
            sql = f"SELECT deja_prete FROM Oeuvre WHERE Oeuvre.ID_Oeuvre = {id};"
            cur.execute(sql)
            res = cur.fetchall()
            print("---------------PRET---------------\n")
            for raw in res:
                if raw[0] == 0:
                    print("L'œuvre n'a jamais été prêtée.\n")
                elif raw[0] == 1:
                    print("L'œuvre a déjà été prêtée.\n")
                else:
                    print("Erreur.\n")

        elif choix == 8:
            nom_exposition = affichageExpo()
            sql = f"SELECT numero FROM SALLE WHERE exposition = '{nom_exposition}';"
            cur.execute(sql)
            res = cur.fetchall()
            print("--------------SALLE EXPOSITION--------------\n")
            for raw in res:
                print(f"Numéro de salle : {raw[0]}\n")

        elif choix == 9:
            idOeuvre = affichageOeuvre()
            sql_nom = f"SELECT titre FROM Oeuvre WHERE id_oeuvre = {idOeuvre};"
            cur.execute(sql_nom)
            nom = cur.fetchone()
            if nom is not None:
                sql = f"SELECT Restauration.type, Restauration.raison_sociale, Restauration.date, Restauration.montant FROM Restauration JOIN Oeuvre ON Restauration.id_oeuvre = Oeuvre.id_oeuvre WHERE Oeuvre.titre = '{nom[0]}';"
                cur.execute(sql)
                res = cur.fetchall()
                print("--------------INFORMATION PRESTATAIRE-----------------\n")
                for raw in res:
                    print(f"Type {raw[0]} | Raison sociale {raw[1]} | Date {raw[2]} | Montant {raw[3]} €\n")


        elif choix == 10:
            sql = "SELECT prix_acquisition, titre FROM Oeuvre JOIN Musee ON Oeuvre.id_musee = Musee.id_musee WHERE Musee.nom = 'Musee du Louvre';"
            cur.execute(sql)
            res = cur.fetchall()
            print("------------------PRIX ACQUISITION------------------\n")
            for raw in res:
                print(f"Œuvre: {raw[1]} | Prix d'acquisition : {raw[0]}€\n")

        elif choix == 11:
            sql = "SELECT titre, date_debut_emprunt, date_fin_emprunt FROM Oeuvre WHERE Oeuvre.deja_emprunte = TRUE;"
            cur.execute(sql)
            res = cur.fetchall()
            print("----DATE EMPRUNT----\n")
            for raw in res:
                print(
                    f"Titre : {raw[0]} | Date du début de l'emprunt : {raw[1]} | Date de la fin de l'emprunt : {raw[2]}\n")

        elif choix == 12:
            sql = "SELECT Musee.nom, Musee.adresse FROM Musee JOIN Oeuvre ON Musee.id_musee = Oeuvre.id_musee JOIN Presente ON Oeuvre.id_oeuvre = Presente.id_oeuvre JOIN Exposition_Temporaire ON Presente.nom_exposition = Exposition_Temporaire.nom WHERE Oeuvre.deja_emprunte = TRUE;"
            cur.execute(sql)
            res = cur.fetchall()
            print("----INFORMATION MUSÉE EXTÉRIEUR----\n")
            for raw in res:
                print(f"Nom : {raw[0]} | Adresse :{raw[1]}\n")

        elif choix == 13:
            nom = affichageExpo()
            sql = f"SELECT Musee.nom, Musee.adresse FROM Musee JOIN Oeuvre ON Musee.id_musee = Oeuvre.id_musee JOIN Presente ON Oeuvre.id_oeuvre = Presente.id_oeuvre JOIN Exposition_Temporaire ON Presente.nom_exposition = Exposition_Temporaire.nom WHERE Oeuvre.deja_emprunte = TRUE AND Exposition_Temporaire.nom= '{nom}';"
            cur.execute(sql)
            res = cur.fetchall()
            print("----INFORMATION MUSÉE EXTÉRIEUR----\n")
            for raw in res:
                print(f"Nom : {raw[0]} | Adresse :{raw[1]} \n")

        elif choix == 14:
            id = int(affichageOeuvre())
            sql = f"SELECT titre, date, dimension, Nom, date_naiss, date_mort FROM Oeuvre JOIN Cree ON Oeuvre.ID_Oeuvre = Cree.ID_Oeuvre JOIN Auteur ON Auteur.ID_Nom = Cree.ID_Auteur WHERE Oeuvre.ID_Oeuvre = {id};"
            cur.execute(sql)
            res = cur.fetchall()
            print("----INFORMATION OEUVRE----\n")
            for raw in res:
                print(f"Titre : {raw[0]} | Date : {raw[1]} | Dimension : {raw[2]} | Nom : {raw[3]} | Date de "
                      f"naissance : {raw[4]} | Date de mort : {raw[5]} \n")
        else:
            print("Veuillez choisir un numéro affiché à l'écran.\n")


def consultationStatistique():
    """Affiche les statistiques disponibles"""
    choix = 4
    while choix != 0:
        print("Voici les statistiques que vous vouliez consulter : \n")
        print("1- Connaître le maximum, le minimum et la moyenne des expositions temporaires.\n")
        print("2- Coût total d’acquisition par musée.\n")
        print("3- Le prix moyen d'acquisition des œuvres des expositions.\n")
        print("0- Quitter\n")
        choix = int(input("Quel est votre choix ?\n"))
        if choix == 1:
            sql = "SELECT MAX(date_fin - date_debut) AS duree_max, MIN(date_fin - date_debut) AS duree_min, AVG(date_fin - date_debut) AS duree_moyenne FROM Exposition_Temporaire;"

            # sql = "SELECT MAX(DATEDIFF(day, date_debut, date_fin)) AS duree_max, MIN(DATEDIFF(day, date_debut, date_fin)) AS duree_min, AVG(DATEDIFF(day, date_debut, date_fin)) AS duree_moyenne FROM Exposition_Temporaire;"
            cur.execute(sql)
            res = cur.fetchall()
            for raw in res:
                print(f"- Durée maximale :{raw[0]}  | Durée minimale :{raw[1]} | Moyenne: {raw[2]}\n")

        elif choix == 2:

            sql = "SELECT  m.nom AS nom_musee, SUM(o.prix_acquisition) AS cout_total_acquisition FROM Oeuvre o JOIN Musee m ON o.ID_Musee = m.ID_Musee GROUP BY m.nom;"
            cur.execute(sql)
            res = cur.fetchall()
            for raw in res:
                print(f"Musée : {raw[0]} ---- Coût total d'acquisition : {raw[1]} €\n")

        elif choix == 3:
            sql = "SELECT p.nom_exposition, AVG(o.prix_acquisition) AS prix_moyen_acquisition FROM Oeuvre o JOIN Presente p ON o.ID_oeuvre = p.ID_oeuvre GROUP BY p.nom_exposition;"
            cur.execute(sql)
            res = cur.fetchall()
            for raw in res:
                print(f"Exposition temporaire : {raw[0]} Prix moyen d'acquisition :{raw[1]}€")

        elif choix == 0:
            print("Merci. Au revoir.\n")
            conn.close()

        else:
            print("Ce choix n'existe pas, veuillez choisir un chiffre parmi ceux affichés à l'écran.\n")
