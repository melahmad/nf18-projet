import psycopg2

try:
    conn = psycopg2.connect("dbname='dbnf18a033' user='nf18a033' password='7OzX1eycMOQL' host='tuxa.sme.utc'")
except psycopg2.OperationalError as e:
    print("Message personnalisé : contrainte non respectée")
    print("Message système :", e)
    conn.rollback()


cur = conn.cursor()


noms_tables_admin = ["Auteur", "Musee", "Oeuvre", "Cree", "Restauration", "Peinture", "Sculpture", "Photo",
                     "Exposition", "Presente", "Exposition_Temporaire", "Exposition_permanente", "Salle",
                     "Panneau_Explicatif", "Creneau", "Guide", "Affecte", "Mene_exp_temp", "Mene_exp_perm"]
colonnes_tables = dict(Auteur=[["ID_nom", "INTEGER"], ["nom", "string"], ["date_naiss", "date"], ["date_mort", "date"]],
                       Musee=[["ID_musee", "INTEGER"], ["nom", "string"], ["adresse", "string"]],
                       Oeuvre=[["ID_oeuvre", "INTEGER"], ["titre", "string"], ["date", "date"], ["dimension", "string"],
                               ["prix_acquisition", "INTEGER"], ["ID_Musee", "INTEGER"], ["date_début_emprunt", "date"],
                               ["date_fin_emprunt", "date"], ["date_début_pret", "date"], ["date_fin_pret", "date"],
                               ["déja_prêté", "bool"], ["déja_emprunté", "bool"]],
                       Restauration=[["ID_Restauration", "INTEGER"], ["date", "date"], ["montant", "DECIMAL"],
                                     ["type", "string"], ["ID_oeuvre", "INTEGER"]],
                       Peinture=[["ID_oeuvre", "INTEGER"]],
                       Sculpture=[["ID_oeuvre", "INTEGER"]],
                       Photo=[["ID_oeuvre", "INTEGER"]],
                       Exposition=[["nom", "string"]],
                       Presente=[["ID_oeuvre", "INTEGER"], ["nom_exposition", "string"]],
                       Exposition_Temporaire=[["nom", "string"], ["date_debut", "date"], ["date_fin", "date"]],
                       Exposition_permanente=[["nom", "string"]],
                       Salle=[["numero", "INTEGER"], ["cap_max", "INTEGER"], ["exposition", "string"]],
                       Panneau_Explicatif=[["num", "INTEGER"], ["texte", "string"], ["salle", "INTEGER"]],
                       Creneau=[["jour", "string"], ["h_debut", "INTEGER"]],
                       Guide=[["id", "INTEGER"], ["nom", "string"], ["prenom", "string"], ["adresse", "string"],
                              ["date_embauche", "date"]],
                       Affecte=[["jour", "string"], ["h_debut", "INTEGER"], ["guide", "INTEGER"]],
                       Mene_exp_temp=[["id", "INTEGER"], ["nom", "string"]],
                       Mene_exp_perm=[["id", "INTEGER"], ["nom", "string"]],
                       Cree=[["ID_Auteur", "INTEGER"], ["ID_Oeuvre", "INTEGER"]])


def ajoutDonneeAdmin():
    """Ajoute des données à une table"""
    print("À quelle table voulez-vous ajouter des données ?")
    for i in range(len(noms_tables_admin)):
        print(f'{i}.{noms_tables_admin[i]}')
    table = int(input("Saisir votre choix : "))
    if 0 <= table < len(noms_tables_admin):
        table_selectionnee = noms_tables_admin[table]
        if table_selectionnee in colonnes_tables:
            colonnes = colonnes_tables[table_selectionnee]
            print(f"Voici les colonnes de la table {table_selectionnee} : \n")
            i = 1
            for c in colonnes:
                print(f"{i}.{c[0]}\n")
                i += 1
            valeurs = []
            sql = f"INSERT INTO {table_selectionnee} VALUES ("
            for nom_colonne, type_colonne in colonnes:
                valeur = input(f"Entrez la valeur que vous voulez ajouter pour la colonne {nom_colonne}: ")
                if type_colonne != "INTEGER" and type_colonne != "DECIMAL":
                    print(type_colonne)
                    valeur = f"'{valeur}'"
                valeurs.append(valeur)
            sql = f"{sql} {', '.join(valeurs)});"
            choix_verif = 2
            while choix_verif != 0 and choix_verif != 1:
                choix_verif = int(input(f"Voici la requête SQL : {sql}\n Voulez-vous l'ajouter ? (0-non, 1-oui)\n"))
                if choix_verif == 1:
                    print("Exécution de la requête SQL en cours.\n")
                    cur.execute(sql)
                    conn.commit()
                    print("Requête SQL exécutée.\n")
                elif choix_verif == 0:
                    print("Annulation de la requête SQL.\n")
                else:
                    print("Veuillez choisir un 1 pour oui ou 0 pour non.\n")
        else:
            print(f"{table_selectionnee} ne se trouve pas dans le dictionnaire. Impossible d'accéder à ses colonnes.\n")
    else:
        print("Erreur lors du choix de la table.\n")



def affichageContenuTable(table):
    """Affiche le contenu d'une table"""
    sql = f"SELECT * FROM {table};"
    cur.execute(sql)
    res = cur.fetchall()
    for nomColonne in colonnes_tables[table]:
        print(f"| {nomColonne[0]}", end=' ')
    print("|")
    print("|", end='')
    for c in colonnes_tables[table]:
        print("---------------", end='')
    print()

    for raw in res:
        for valeur in raw:
            print(f"| {valeur} ", end=' ')
        print("|")

def suppressionDonneesAdmin():
    """Supprime des données à une table"""
    print("À quelle table voulez-vous supprimer des données ?")
    for i in range(len(noms_tables_admin)):
        print(f'{i}.{noms_tables_admin[i]}')
    choix_table = int(input("Saisir votre choix : "))
    if 0 <= choix_table < len(noms_tables_admin):
        table_selectionnee = noms_tables_admin[choix_table]
        print(f"Voici les données de la table {table_selectionnee} :\n")
        affichageContenuTable(table_selectionnee)
        cond = input("Quelle est la condition de suppression ? ")
        sql = f"DELETE FROM {table_selectionnee} WHERE {cond};"
        choix_verif = 2
        while choix_verif != 0 and choix_verif != 1:
            choix_verif = int(input(f"Voici la requête SQL : {sql}\n Voulez- vous continuer ? (0-non, 1-oui)\n"))
            if choix_verif == 1:
                print("Exécution de la requête sql en cours.\n")
                cur.execute(sql)
                conn.commit()
                print("Requête SQL exécutée.\n")
            elif choix_verif == 0:
                print("Annulation de la requête sql.\n")
            else:
                print("Veuillez choisir un 1 pour oui ou 0 pour non.\n")
        else:
            print("Erreur lors du choix de la table.\n")


def affichageTableAdmin():
    """Affiche les noms des tables qu'un administrateur peut consulter et affiche son contenu"""
    print("Quel table voulez vous consulter ?")
    for i in range(len(noms_tables_admin)):
        print(f'{i}.{noms_tables_admin[i]}')
    choix_table = int(input("Saisir votre choix :"))
    if 0 <= choix_table < len(noms_tables_admin):
        table_selectionnee = noms_tables_admin[choix_table]
        affichageContenuTable(table_selectionnee)
    else:
        print("Erreur lors du choix de la table.\n")


def creationTableAdmin():
    """Crée une nouvelle table"""
    nom_table = input("Entrez le nom de la nouvelle table : ")
    if nom_table.lower() not in [table.lower() for table in noms_tables_admin]:
        colonnes = []
        i = 1
        while 1:
            col = input(f"Entrez le nom de la colonne {i} :")
            i += 1
            while True:
                type_col = input(f"Entrez le type de données pour {col} (ex: VARCHAR, INTEGER, DATE ...) : ")
                if type_col:
                    break
                else:
                    print("Veuillez saisir un type de données.\n")
            while True:
                continuer = input("Voulez-vous ajouter d'autres colonnes ? (0-non / 1-oui): ")
                if continuer == '0' or continuer == '1':
                    break
                else:
                    print("Veuillez saisir 0 ou 1.\n")
            if continuer == '0':
                colonnes.append(col)
                break
            elif continuer == '1':
                colonnes.append(col)
        print(colonnes)
        choix_inf = int(input("Voulez vous ajouter d'autre informations (ex: qui est la clé primaire, les clés "
                              "étrangères...) ? (0-non / 1-oui)"))
        colonnes_str = ', '.join(f'{col} {type_col}' for col in colonnes)
        sql = f"CREATE TABLE {nom_table} ({colonnes_str}"
        while True:
            if choix_inf == 0 or choix_inf == 1:
                break
            else:
                print("Veuillez saisir 0 ou 1.\n")
        if choix_inf == 0:
            sql = f"{sql});"
        elif choix_inf == 1:
            ajout = input("Ajouter l'information : ")
            sql = f"{sql} , {ajout});"
        choix_verif = int(input(f"Voici la requête SQL : {sql}\n Voulez- vous continuer ? (0-non, 1-oui)\n"))
        if choix_verif == 1:
            print("Exécution de la requête sql en cours.\n")
            cur.execute(sql)
            conn.commit()
            print("Requête SQL exécutée.\n")
            noms_tables_admin.append(nom_table)
            colonnes_tables[nom_table] = [[col, type_col] for col in colonnes]
        elif choix_verif == 0:
            print("Annulation de la requête sql.\n")
        else:
            print("Veuillez choisir un 1 pour oui ou 0 pour non.\n")
    else:
        print("Erreur, il existe déjà une table avec ce nom.\n")


def suppressionTableAdmin():
    """Supprimer une table existante"""
    print("Quelle table voulez-vous supprimer")
    for i in range(len(noms_tables_admin)):
        print(f'{i}.{noms_tables_admin[i]}')
    choix_table = int(input("Saisir votre choix : "))
    if 0 <= choix_table < len(noms_tables_admin):
        table_selectionnee = noms_tables_admin[choix_table]
        sql = f"DROP TABLE {table_selectionnee};"
        choix_verif = int(input(f"Voici la requête SQL : {sql}\n Voulez- vous continuer ? (0-non, 1-oui)\n"))
        if choix_verif == 1:
            print("Exécution de la requête sql en cours.\n")
            cur.execute(sql)
            conn.commit()
            print("Requête SQL exécutée.\n")
            del noms_tables_admin[choix_table]
            del colonnes_tables[table_selectionnee]
        elif choix_verif == 0:
            print("Annulation de la requête sql.\n")
        else:
            print("Veuillez choisir un 1 pour oui ou 0 pour non.\n")
    else:
        print("La table n'existe pas.\n")

    conn.commit()
