import psycopg2
from datetime import date

try:
    conn = psycopg2.connect("dbname='dbnf18a033' user='nf18a033' password='7OzX1eycMOQL' host='tuxa.sme.utc'")
except psycopg2.OperationalError as e:
    print("Message personnalisé : contrainte non respectée")
    print("Message système :", e)
    conn.rollback()

cur = conn.cursor()


def affichageExpo():
    """Affiche toutes les expositions"""
    sql = "SELECT nom FROM Exposition;"
    cur.execute(sql)
    res = cur.fetchall()
    print("-----------------EXPOSITION------------------\n")
    i = 1
    expositions = []
    for raw in res:
        print(f"{i}- {raw[0]}\n")
        expositions.append(raw[0])
        i += 1
    choix_expo = ""
    while choix_expo not in expositions:
        try:
            choix_idE = int(input("Quelle exposition voulez vous consulter? Entrez le numéro de l'exposition : "))
            if 1 <= choix_idE <= len(expositions):
                choix_expo = expositions[choix_idE - 1]
            else:
                print("Veuillez entrer un numéro valide.\n")
        except ValueError:
            print("Veuillez entrer un nombre entier.\n")
    return choix_expo


def affichageOeuvre():
    """Affiche toutes les œuvres"""
    sql = "SELECT ID_oeuvre, titre FROM Oeuvre;"
    cur.execute(sql)
    res = cur.fetchall()
    print("-------------------ŒUVRES-----------------------")
    idOeuvres = []
    for raw in res:
        print(f"{raw[0]}- {raw[1]}\n")
        idOeuvres.append(raw[0])
    choix_oeuvre = 0
    while choix_oeuvre not in idOeuvres:
        try:
            choix_oeuvre = int(input("Quelle œuvre voulez-vous consulter? Entrez l'ID de l'œuvre : "))
            if choix_oeuvre not in idOeuvres:
                print("Veuillez sélectionner un ID d'œuvre valide.\n")
        except ValueError:
            print("Veuillez entrer un nombre entier.\n")
    return choix_oeuvre


def voir_expositions():
    """Affiche les expositions en cours"""
    sql1 = "SELECT * FROM Exposition_permanente;"
    cur.execute(sql1)
    raw = cur.fetchone()
    print("Voici les expositions en cours :\n")
    print(" Nom de l\'exposition permanente ")
    while raw:
        print(" %s " % (raw[0]))
        raw = cur.fetchone()
    today = date.today()
    print()
    sql2 = "SELECT * FROM Exposition_Temporaire WHERE date_fin > NOW() AND date_debut < NOW();"
    cur.execute(sql2)
    raw = cur.fetchone()
    while raw:
        print(" Nom de l'exposition temporaire : %s   |  Date début : %s |  Date fin : %s " % (raw[0], raw[1], raw[2]))
        raw = cur.fetchone()


def salles_exposition():
    """Affiche les salles d'une exposition"""
    exposition = affichageExpo()
    sql = "SELECT numero FROM SALLE WHERE exposition = '%s';" % (exposition)
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        print("Salles pour %s " % exposition)
        while raw:
            print(raw[0])
            raw = cur.fetchone()
    else:
        print("Il n'y a pas de salle avec l'exposition donnée.")


def info_oeuvre():
    """Affiche les informations des œuvres"""
    idOeuvre = affichageOeuvre()
    sql_nom = f"SELECT titre FROM Oeuvre WHERE id_oeuvre = {idOeuvre};"
    cur.execute(sql_nom)
    oeuvre = cur.fetchone()
    sql = "SELECT titre, date, dimension, Nom FROM Oeuvre JOIN Cree ON Oeuvre.ID_Oeuvre = Cree.ID_Oeuvre JOIN Auteur ON Auteur.ID_Nom = Cree.ID_Auteur WHERE Oeuvre.titre = '%s';" % (
        oeuvre)
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        while raw:
            print(" Titre : %s |  Date : %s | Dimension : %s | Auteur : %s " % (raw[0], raw[1], raw[2], raw[3]))
            raw = cur.fetchone()
    else:
        print("Il n'y a pas d'œuvre avec le nom donné.\n")


def affichageTableVisiteur():
    """Affiche les noms des tables qu'un visiteur peut consulter"""
    choix = 4
    while choix != 0:
        print("Que voulez vous consulter ?\n")
        print("1. Les expositions en cours.\n"
              "2. Les salles dédiées à une exposition temporaire recherchée.\n"
              "3. Connaître les informations des œuvres.\n"
              "0. Quitter\n")
        choix = int(input("Entrez votre choix : "))
        if choix == 1:
            voir_expositions()
        elif choix == 2:
            salles_exposition()
        elif choix == 3:
            info_oeuvre()
        elif choix == 0:
            print("Merci pour votre visite. À bientôt !\n")
            cur.close()
            conn.close()
        else:
            print("Choix impossible, veuillez choisir un numéro parmi ceux proposés.\n")


if __name__ == '__main__':
    affichageTableVisiteur()
