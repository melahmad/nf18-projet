import psycopg2

try:
    conn = psycopg2.connect("dbname='dbnf18a033' user='nf18a033' password='7OzX1eycMOQL' host='tuxa.sme.utc'")
except psycopg2.OperationalError as e:
    print("Message personnalisé : contrainte non respectée")
    print("Message système :", e)
    conn.rollback()

cur = conn.cursor()


def affichageOeuvre():
    """Affiche toutes les œuvres"""
    sql = "SELECT ID_oeuvre, titre FROM Oeuvre;"
    cur.execute(sql)
    res = cur.fetchall()
    print("-----------------ŒUVRES-----------------")
    idOeuvres = []
    for raw in res:
        print(f"{raw[0]}- {raw[1]}\n")
        idOeuvres.append(raw[0])
    choix_oeuvre = 0
    while choix_oeuvre not in idOeuvres:
        try:
            choix_oeuvre = int(input("Quelle œuvre voulez-vous consulter? Entrez l'ID de l'œuvre : "))
            if choix_oeuvre not in idOeuvres:
                print("Veuillez sélectionner un ID d'œuvre valide.\n")
        except ValueError:
            print("Veuillez entrer un nombre entier.\n")
    return choix_oeuvre


def affichageRaisonSociale():
    """Affiche toutes les raisons sociales"""
    sql = "SELECT DISTINCT raison_sociale FROM Restauration;"
    cur.execute(sql)
    res = cur.fetchall()
    print("----------------RAISON SOCIALE------------------\n")
    i = 1
    raisonSociale = []
    for raw in res:
        print(f"{i}- {raw[0]}\n")
        raisonSociale.append(raw[0])
        i += 1
    choix_RS = ""
    while choix_RS not in raisonSociale:
        try:
            choix_idRS = int(input("Quelle est votre raison sociale ? Entrez le numéro de la raison sociale : "))
            if 1 <= choix_idRS <= len(raisonSociale):
                choix_RS = raisonSociale[choix_idRS - 1]
            else:
                print("Veuillez entrer un numéro valide.\n")
        except ValueError:
            print("Veuillez entrer un nombre entier.\n")
    return choix_RS


def historique_restauration():
    """Affiche l'historique de restauration d'une œuvre"""
    print("Veuillez saisir l'ID de l'œuvre dont vous voulez connaître l'historique de restauration:\n")
    ID = affichageOeuvre()
    sql = "SELECT date, type, raison_sociale FROM Restauration WHERE id_oeuvre = '%s';" % (ID)
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        print("Voici l'historique des restaurations de l'œuvre :\n")
        while raw:
            print(" Date : %s  | Type : %s | Raison sociale : %s " % (raw[0], raw[1], raw[2]))
            print("---------------------------------------------------------------------------------------------------")
            raw = cur.fetchone()
    else:
        print("Aucune restauration pour cette œuvre:\n")


def restaurations(raison_sociale):
    """Affiche les restaurations effectuées par un restaurateur"""
    sql = "SELECT date, montant, type, ID_oeuvre FROM Restauration WHERE raison_sociale= '%s';" % (raison_sociale)
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        print("Voici vos restaurations :\n")
        while raw:
            print("Date :  %s | Montant : %s€ | Type : %s | ID_oeuvre : %s " % (raw[0], raw[1], raw[2], raw[3]))
            raw = cur.fetchone()
    else:
        print("Vous n'avez pas des restaurations.\n")


def affichageTableRestaurateur():
    """Affiche les noms des tables qu'un restaurateur peut consulter"""
    choix = 4
    print("Quel est votre raison sociale ?")
    raison_sociale = affichageRaisonSociale()

    while choix != 0:
        print("Que voulez-vous consulter ?\n")
        print("1. Vos restaurations.\n"
              "2.L'historique des restaurations d'une œuvre.\n"
              "0. Quitter\n")
        choix = int(input("Entrez votre choix : "))
        if choix == 1:
            restaurations(raison_sociale)
        elif choix == 2:
            historique_restauration()
        elif choix == 0:
            print("Merci pour votre visite. À bientôt !\n")
            cur.close()
            conn.close()
        else:
            print("Choix impossible, veuillez choisir un numéro parmi ceux proposés.\n")
