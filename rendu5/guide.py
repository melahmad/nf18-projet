import psycopg2

try:
    conn = psycopg2.connect("dbname='dbnf18a033' user='nf18a033' password='7OzX1eycMOQL' host='tuxa.sme.utc'")
except psycopg2.OperationalError as e:
    print("Message personnalisé : contrainte non respectée")
    print("Message système :", e)
    conn.rollback()

cur = conn.cursor()


def crenaux(nom, prenom):
    """Affiche les créneaux de travail pour un guide donné"""
    sql = f"SELECT jour, h_debut FROM Affecte JOIN Guide ON Affecte.guide = Guide.id  WHERE Guide.nom = '{nom}' AND Guide.prenom = '{prenom}';"
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        print("Voici vos créneaux :\n")
        while raw:
            print(f" Jour : {raw[0]}  | Heure :  {raw[1]} ")
            raw = cur.fetchone()
    else:
        print("Il n'y a pas de guide avec le nom et prénom donné.\n")


def affichageExpoPermanente():
    """Affiche toutes les expositions permanentes"""
    sql = "SELECT nom FROM Exposition_permanente;"
    cur.execute(sql)
    res = cur.fetchall()
    print("--------EXPOSITION PERMANENTE--------\n")
    i = 1
    expositions = []
    for raw in res:
        print(f"{i}- {raw[0]}\n")
        expositions.append(raw[0])
        i += 1
    choix_expo = ""
    while choix_expo not in expositions:
        try:
            choix_idE = int(input("Quelle exposition voulez vous consulter? Entrez le numéro de l'exposition : "))
            if 1 <= choix_idE <= len(expositions):
                choix_expo = expositions[choix_idE - 1]
            else:
                print("Veuillez entrer un numéro valide.\n")
        except ValueError:
            print("Veuillez entrer un nombre entier.\n")
    return choix_expo


def guide_exp_perm():
    """Affiche les guides d'une exposition permanente"""
    exp = affichageExpoPermanente()
    sql = f"SELECT Guide.nom, Guide.prenom FROM Mene_exp_perm JOIN Guide ON Guide.id = Mene_exp_perm.id JOIN Exposition_permanente ON Exposition_permanente.nom = Mene_exp_perm.nom WHERE Exposition_permanente.nom = '{exp}';"
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        while raw:
            print(f" Nom : {raw[0]}  | Prénom : {raw[1]} ")
            print("---------------------------------------")
            raw = cur.fetchone()
    else:
        print("Il n'y a pas d'exposition avec le nom donné.\n")


def affichageTableGuide():
    """Affiche les noms des tables qu'un guide peut consulter"""
    choix = 4
    nom = input("Quel est votre nom ?")
    prenom = input("Quel est votre prénom ?")
    while choix != 0:
        print("Que voulez-vous consulter ?\n")
        print("1. Qui mène une exposition permanente.\n"
              "2. À quel créneau vous devez travailler.\n"
              "0. Quitter\n")
        choix = int(input("Entrez votre choix : "))
        if choix == 1:
            guide_exp_perm()
        elif choix == 2:
            crenaux(nom, prenom)
        elif choix == 0:
            print("Merci pour votre visite. À bientôt !\n")
            cur.close()
            conn.close()
        else:
            print("Choix impossible, veuillez choisir un numéro parmi ceux proposés.\n")
