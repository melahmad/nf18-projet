/*CREATE TABLE Auteur (
  ID_Nom INT PRIMARY KEY,
  Nom VARCHAR(25) NOT NULL,
  date_naiss DATE NOT NULL,
  date_mort DATE,
  CHECK (date_naiss < date_mort)
);*/

CREATE TABLE Musee (
  ID_Musee INT PRIMARY KEY,
  nom VARCHAR(25) NOT NULL,
  adresse VARCHAR(100) NOT NULL
);
/* ANCIENNE TABLE OEUVRE :
CREATE TABLE Oeuvre (
  ID_oeuvre INT PRIMARY KEY,
  titre VARCHAR(100) NOT NULL,
  date DATE NOT NULL,
  dimension VARCHAR(15) NOT NULL,
  prix_acquisition INT NOT NULL,
  ID_Musee INT,
  date_debut_emprunt DATE,
  date_fin_emprunt DATE,
  date_debut_pret DATE,
  date_fin_pret DATE,
  deja_prete BOOLEAN,
  deja_emprunte BOOLEAN,
  FOREIGN KEY (ID_Musee) REFERENCES Musee(ID_Musee),
  CHECK (date_debut_emprunt < date_fin_emprunt),
  CHECK (date_debut_pret < date_fin_pret)
);
*/

/*NOUVELLE TABLE OEUVRE : */
CREATE TABLE Oeuvre (
  ID_oeuvre INT PRIMARY KEY,
  titre VARCHAR(100) NOT NULL,
  date DATE NOT NULL,
  dimension VARCHAR(15) NOT NULL,
  prix_acquisition INT NOT NULL,
  ID_Musee INT,
  date_debut_emprunt DATE,
  date_fin_emprunt DATE,
  date_debut_pret DATE,
  date_fin_pret DATE,
  deja_prete BOOLEAN,
  deja_emprunte BOOLEAN,
  auteur JSON NOT NULL,
  FOREIGN KEY (ID_Musee) REFERENCES Musee(ID_Musee),
  CHECK (date_debut_emprunt < date_fin_emprunt),
  CHECK (date_debut_pret < date_fin_pret)
);

/*
CREATE TABLE Cree (
  ID_Auteur INT,
  ID_Oeuvre INT,
  PRIMARY KEY (ID_Auteur, ID_Oeuvre),
  FOREIGN KEY (ID_Auteur) REFERENCES Auteur(ID_Nom),
  FOREIGN KEY (ID_Oeuvre) REFERENCES Oeuvre(ID_oeuvre)
);
*/
CREATE TABLE Restauration (
  ID_Restauration INT PRIMARY KEY,
  date DATE NOT NULL,
  montant DECIMAL(7, 2) NOT NULL,
  type VARCHAR(25) NOT NULL,
  ID_oeuvre INT,
  raison_sociale VARCHAR(30) NOT NULL,
  FOREIGN KEY (ID_oeuvre) REFERENCES Oeuvre(ID_oeuvre)
);

CREATE TABLE Peinture (
  ID_oeuvre INT PRIMARY KEY,
  FOREIGN KEY (ID_oeuvre) REFERENCES Oeuvre(ID_oeuvre)
);

CREATE TABLE Sculpture (
  ID_oeuvre INT PRIMARY KEY,
  FOREIGN KEY (ID_oeuvre) REFERENCES Oeuvre(ID_oeuvre)
);

CREATE TABLE Photo (
  ID_oeuvre INT PRIMARY KEY,
  FOREIGN KEY (ID_oeuvre) REFERENCES Oeuvre(ID_oeuvre)
);

CREATE TABLE Exposition (
  nom VARCHAR(100) PRIMARY KEY
);

CREATE TABLE Presente (
  ID_oeuvre INT,
  nom_exposition VARCHAR(50),
  PRIMARY KEY (ID_oeuvre, nom_exposition),
  FOREIGN KEY (ID_oeuvre) REFERENCES Oeuvre(ID_oeuvre),
  FOREIGN KEY (nom_exposition) REFERENCES Exposition(nom)
);
/*
ANCIENNE TABLE EXPOSITION TEMPORRAIRE
CREATE TABLE Exposition_Temporaire (
  nom VARCHAR(50),
  date_debut DATE NOT NULL,
  date_fin DATE NOT NULL,
  PRIMARY KEY (nom),
  FOREIGN KEY (nom) REFERENCES Exposition(nom),
  CHECK (date_debut < date_fin)
);
*/

/*NOUVELLE TABLE EXPOSITION TEMPORAIRE*/
CREATE TABLE Exposition_Temporaire (
  nom VARCHAR(50),
  date_debut DATE NOT NULL,
  date_fin DATE NOT NULL,
  guide JSON,
  PRIMARY KEY (nom),
  FOREIGN KEY (nom) REFERENCES Exposition(nom),
  CHECK (date_debut < date_fin)
);

/*
ANCIENNE TABLE EXPOSITION PERMANENTE
CREATE TABLE Exposition_permanente (
  nom VARCHAR(50),
  PRIMARY KEY (nom),
  FOREIGN KEY (nom) REFERENCES Exposition(nom)
);
*/
/*NOUVELLE TABLE EXPOSITION TEMPORAIRE*/
CREATE TABLE Exposition_permanente (
  nom VARCHAR(50),
  PRIMARY KEY (nom),
  guide JSON NOT NULL,
  FOREIGN KEY (nom) REFERENCES Exposition(nom)
);
/*ANCIENNE TABLE SALLE :
CREATE TABLE Salle (
  numero INT PRIMARY KEY,
  cap_max INT NOT NULL ,
  exposition VARCHAR(50),
  FOREIGN KEY (exposition) REFERENCES Exposition_Temporaire(nom),
  CHECK (cap_max > 0)
);
*/
/*NOUVELLE TABLE SALLE : */
CREATE TABLE Salle (
  numero INT PRIMARY KEY,
  cap_max INT NOT NULL ,
  exposition VARCHAR(50),
  Panneau_Explicatif JSON NOT NULL,
  FOREIGN KEY (exposition) REFERENCES Exposition_Temporaire(nom),
  CHECK (cap_max > 0)
);
/*
CREATE TABLE Panneau_Explicatif (
  num INT PRIMARY KEY,
  texte VARCHAR(300) NOT NULL,
  salle INT NOT NULL,
  FOREIGN KEY (salle) REFERENCES Salle(numero)
);
*/
/*
CREATE TABLE Creneau (
  jour VARCHAR(8),
  h_debut INT,
  PRIMARY KEY (jour, h_debut)
);
*/
/*
CREATE TABLE Guide (
  id INT PRIMARY KEY,
  nom VARCHAR(25) NOT NULL,
  prenom VARCHAR(25) NOT NULL,
  adresse VARCHAR(50) NOT NULL,
  date_embauche DATE NOT NULL
);
*/
/*
CREATE TABLE Affecte (
  jour VARCHAR(8),
  h_debut INT,
  guide INT,
  PRIMARY KEY (jour, h_debut, guide),
  FOREIGN KEY (jour, h_debut) REFERENCES Creneau(jour, h_debut),
  FOREIGN KEY (guide) REFERENCES Guide(id)
);
*/
/*
CREATE TABLE Mene_exp_temp (
  id INT,
  nom VARCHAR(50),
  PRIMARY KEY (id, nom),
  FOREIGN KEY (id) REFERENCES Guide(id),
  FOREIGN KEY (nom) REFERENCES Exposition_Temporaire(nom)
);
*/
/*
CREATE TABLE Mene_exp_perm (
  id INT,
  nom VARCHAR(50),
  PRIMARY KEY (id, nom),
  FOREIGN KEY (id) REFERENCES Guide(id),
  FOREIGN KEY (nom) REFERENCES Exposition_permanente(nom)
);
*/
/*
INSERT INTO Auteur (ID_Nom, Nom, date_naiss, date_mort) VALUES
(1, 'Auguste Rodin', '1840-11-12', '1917-11-17'),
(2, 'Antonio Canova', '1757-11-01', '1822-10-13'),
(3, 'Johannes Vermeer', '1632-10-31', '1675-12-15'),
(4, 'Claude Monet', '1840-11-14', '1926-12-05'),
(5, 'Paul Véronèse', '1528-03-30', '1588-04-19'),
(6, 'Annie Leibovitz', '1949-10-2', NULL),
(7, 'Léonard de Vinci', '1452-04-15', '1519-05-02'),
(8, 'Pablo Picasso', '1881-10-25', '1973-04-08'),
(9, 'Paul Cezanne', '1839-01-19', '1906-10-22'),
(10, 'Parmigianino', '1503-01-11', '1540-08-24'),
(11, 'Henri Cartier Bressoni', '1908-08-22', '2004-08-03'),
(12, 'Eugène Delacroix', '1798-04-26', '1863-08-13'),
(13, 'Theodore Géricault', '1791-10-26', '1824-01-26');
*/

INSERT INTO Musee (ID_Musee, nom, adresse) VALUES
(1, 'Musee du Louvre', '93 rue de Rivoli, 75001 Paris'),
(2, 'Musee d Orsay', 'Esplanade Valery Giscard d Estaing, 75007 Paris'),
(3, 'Centre Pompidou', 'Place Georges-Pompidou, 75004 Paris'),
(4, 'Musee Rodin', '77 Rue de Varenne, 75007 Paris'),
(5, 'Musee Picasso', '5 Rue de Thorigny, 75003 Paris'),
(6, 'Musee Marmottan Monet', '2 Rue Louis Boilly, 75016 Paris'),
(7, 'Musee de l Orangerie', 'Jardin Tuileries, 75001 Paris'),
(8, 'Musee du Quai Branly', '37 Quai Jacques Chirac, 75007 Paris'),
(9, 'Musee Carnavalet', '23 Rue de Sevigne, 75003 Paris'),
(10, 'Musee des Arts Decoratifs', '107 Rue de Rivoli, 75001 Paris'),
(11, 'Musee de Capodimonte', 'Via Milano 2, 80131 Napoli');

INSERT INTO Exposition (nom) VALUES
('Impressionnisme'),
('Classiques du 20e siecle'),
('Art du Moyen Age'),
('Renaissance Italienne'),
('Modernistes'),
('Le trésor de Notre Dame de Paris'),
('Cubisme'),
('Exposition surréaliste'),
('Photographie portrait'),
('Naples à Paris');

/*ANCIEN INSERT EXPOSITION TEMPORAIRE
INSERT INTO Exposition_Temporaire (nom, date_debut, date_fin) VALUES
('Impressionnisme', '2023-06-01', '2023-06-30'),
('Renaissance Italienne', '2024-02-01', '2024-04-15'),
('Modernistes', '2023-05-15', '2023-10-05'),
('Le trésor de Notre Dame de Paris', '2023-10-18', '2024-01-29'),
('Naples à Paris', '2023-06-07', '2024-01-08'),
('Photographie portrait', '2023-09-15', '2024-04-08');
*/
/*NOUVEL INSERT EXPOSITION TEMPORAIRE*/
INSERT INTO Exposition_Temporaire (nom, date_debut, date_fin, guide) VALUES

('Impressionnisme', '2023-06-01', '2023-06-30', '[
{"id":1, "nom":"Durand", "prenom":"Julie",  "adresse":"45 Allee des Tulipes, 75014 Paris",  "date_embauche": "2021-05-08", "crenaux": []} ]'),

('Renaissance Italienne', '2024-02-01', '2024-04-15', '[
{"id":8, "nom":"Leroy", "prenom":"Adam",  "adresse":"41 Rue des Ecoles, 75020 Paris",  "date_embauche": "2021-07-02", "crenaux": []} ]'),

('Modernistes', '2023-05-15', '2023-10-05', '[
{"id":5, "nom":"Moreau", "prenom":"Sarah",  "adresse":"17 Chemin des Vignes, 75008 Paris",  "date_embauche": "2018-07-20", "crenaux":[]} ]'),

('Le trésor de Notre Dame de Paris', '2023-10-18', '2024-01-29', '[
{"id":2, "nom":"Guillot", "prenom":"Emilie",  "adresse":"29 Boulevard des Fleurs, 75016 Paris",  "date_embauche": "2019-09-15", "crenaux": []} ]'),

('Naples à Paris', '2023-06-07', '2024-01-08', '[
{"id":9, "nom":"Huet", "prenom":"Gabriel",  "adresse":"25 Avenue du Maine, 75015 Paris",  "date_embauche": "2022-09-01", "crenaux": [] } ]'),

('Photographie portrait', '2023-09-15', '2024-04-08', '[
{"id":4, "nom":"Garnier", "prenom":"Lucie",  "adresse":"8 Place de la Victoire, 75003 Paris",  "date_embauche": "2018-11-30", "crenaux": []} ]');
/*ANCIEN INSERT POUR LA TABLE EXPOSITION PERMANENTE
INSERT INTO Exposition_permanente (nom) VALUES
('Classiques du 20e siecle'),
('Art du Moyen Age'),
('Cubisme'),
('Exposition surréaliste');
*/
/*NOUVEL INSERT POUR LA TABLE EXPOSITION PERMANENTE*/
INSERT INTO Exposition_permanente (nom, guide) VALUES
('Classiques du 20e siecle',
'{"id":2, "nom":"Guillot", "prenom":"Emilie",  "adresse":"29 Boulevard des Fleurs, 75016 Paris",  "date_embauche": "2019-09-15", "crenaux": [{"jour":"Mardi", "h_debut":14 },
  						                                                                                                                               {"jour":"Lundi", "h_debut":15 }]}'),

('Art du Moyen Age',
'{"id":3, "nom":"Lefevre", "prenom":"Léo",  "adresse":"32 Avenue de la Republique, 75008 Paris",  "date_embauche": "2022-03-21", "crenaux":[{"jour":"Mercredi", "h_debut":9 },
  						                                                                                                                              {"jour":"Jeudi", "h_debut":12 }]}'),

('Cubisme',
'{"id":6, "nom":"Roux", "prenom":"Louis",  "adresse":"26 Rue de la Gare, 75004 Paris",  "date_embauche": "2020-04-12", "crenaux":  [{"jour":"Lundi", "h_debut":14 }]}'),

('Exposition surréaliste',
'{"id":7, "nom":"Perez", "prenom":"Celine",  "adresse":"36 Cours de l Europe, 75002 Paris",  "date_embauche": "2023-01-15", "crenaux": [{"jour":"Jeudi",  "h_debut":9 }]}');


/* ANCIEN INSERT POUR LA TABLE OEUVRE :
INSERT INTO Oeuvre (ID_oeuvre, titre, date, dimension, prix_acquisition, ID_Musee, date_debut_emprunt, date_fin_emprunt, date_debut_pret, date_fin_pret, deja_prete, deja_emprunte) VALUES
(1, 'La Joconde', '1503-01-01', '77x53', 420000000, 4, NULL, NULL, '2022-01-15', '2022-02-15', TRUE, FALSE),
(2, 'Le Penseur', '1880-01-01', '189x98x140', 10700000, 4, '2023-03-01', '2023-03-31', NULL, NULL, FALSE, TRUE),
(3, 'La Mort de Sardanapale', '1827-01-01', '392x496', 100000000, 3, NULL, NULL, '2023-08-01', '2023-10-31', TRUE, FALSE),
(4, 'Guernica', '1937-01-01', '349x777', 200000000, 2, '2023-05-20', '2023-06-20', NULL, NULL, FALSE, TRUE),
(5, 'Psyché ranimée par le baiser de l Amour', '1793-01-01', '155x168x101', 110000000, 5, '2023-05-20', '2023-06-20', NULL, NULL, TRUE, FALSE),
(6, 'Les Joueurs de Cartes', '1895-01-01', '47x56', 250000000, 2, NULL, NULL,'2024-01-10', '2024-02-10', TRUE, FALSE),
(7, 'Mystérieuse Antea', '1535-01-01', '136x86', 104000000, 11, '2023-08-15', '2023-09-15', NULL, NULL, FALSE, TRUE),
(8, 'Queen Elizabeth II', '2007-01-01', '91x137', 6000, 8, '2025-02-01', '2025-03-01', NULL, NULL, FALSE, TRUE),
(9, 'Les Noces de Cana', '1563-01-01', '677×994', 15000000 , 7, NULL, NULL, NULL, NULL, FALSE, FALSE),
(10, 'Le Radeau de La Méduse', '1819-01-01', '491×716', 500000, 8, NULL, NULL, NULL, NULL, FALSE, FALSE),
(11, 'La Liberté guidant le peuple', '1830-01-01', '260 × 325', 12000000, 6, NULL, NULL, NULL, NULL, FALSE, FALSE);
*/
/*NOUVEL INSERT POUR LA TABLE OEUVRE : */
INSERT INTO Oeuvre VALUES
(1, 'La Joconde', '1503-01-01', '77x53', 420000000, 4, NULL, NULL, '2022-01-15', '2022-02-15', TRUE, FALSE,'[
{"ID_Nom":7,"Nom":"Léonard de Vinci", "date_naiss":"1452-04-15","date_mort": "1519-05-02"}
]'
),
(2, 'Le Penseur', '1880-01-01', '189x98x140', 10700000, 4, '2023-03-01', '2023-03-31', NULL, NULL, FALSE, TRUE,'[
{"ID_Nom":1,"Nom":"Auguste Rodin", "date_naiss":"1840-11-12","date_mort": "1917-11-17"}
]'
 ),
(3, 'La Mort de Sardanapale', '1827-01-01', '392x496', 100000000, 3, NULL, NULL, '2023-08-01', '2023-10-31', TRUE, FALSE,'[
{"ID_Nom":12,"Nom":"Eugène Delacroix", "date_naiss":"1798-04-26","date_mort": "1863-08-13"}
]'),
(4, 'Guernica', '1937-01-01', '349x777', 200000000, 2, '2023-05-20', '2023-06-20', NULL, NULL, FALSE, TRUE,'[
{"ID_Nom":8,"Nom":"Pablo Picasso", "date_naiss":"1881-10-25", "date_mort": "1973-04-08"}
]'),
(5, 'Psyché ranimée par le baiser de l Amour', '1793-01-01', '155x168x101', 110000000, 5, '2023-05-20', '2023-06-20', NULL, NULL, TRUE, FALSE,'[
{"ID_Nom":2,"Nom":"Antonio Canova", "date_naiss":"1757-11-01", "date_mort": "1822-10-13"}
]'),
(6, 'Les Joueurs de Cartes', '1895-01-01', '47x56', 250000000, 2, NULL, NULL,'2024-01-10', '2024-02-10', TRUE, FALSE,'[
{"ID_Nom":9,"Nom":"Paul Cezanne", "date_naiss":"1839-01-19", "date_mort": "1906-10-22"}
]'),
(7, 'Mystérieuse Antea', '1535-01-01', '136x86', 104000000, 11, '2023-08-15', '2023-09-15', NULL, NULL, FALSE, TRUE,'[
{"ID_Nom":10,"Nom":"Parmigianino", "date_naiss":"1503-01-11", "date_mort": "1540-08-24"}
]'),
(8, 'Queen Elizabeth II', '2007-01-01', '91x137', 6000, 8, '2025-02-01', '2025-03-01', NULL, NULL, FALSE, TRUE,'[
{"ID_Nom":6,"Nom":"Annie Leibovitz", "date_naiss":"1949-10-02",  "date_mort": "NULL"}
]'),
(9, 'Les Noces de Cana', '1563-01-01', '677×994', 15000000 , 7, NULL, NULL, NULL, NULL, FALSE, FALSE,'[
{"ID_Nom":5,"Nom":"Paul Véronèse", "date_naiss":"1528-03-30", "date_mort": "1588-04-19"}
]'),
(10, 'Le Radeau de La Méduse', '1819-01-01', '491×716', 500000, 8, NULL, NULL, NULL, NULL, FALSE, FALSE,'[
{"ID_Nom":13,"Nom":"Theodore Géricault", "date_naiss":"1791-10-26", "date_mort": "1824-01-26"}
]'),
(11, 'La Liberté guidant le peuple', '1830-01-01', '260 × 325', 12000000, 6, NULL, NULL, NULL, NULL, FALSE, FALSE,'[
{"ID_Nom":12,"Nom":"Eugène Delacroix", "date_naiss":"1798-04-26", "date_mort": "1863-08-13"}
]');

/*
INSERT INTO Cree (ID_Auteur, ID_Oeuvre) VALUES
(1, 2),
(2, 5),
(5, 9),
(6, 8),
(7, 1),
(8, 4),
(9, 6),
(10, 7),
(12, 3),
(12, 11),
(13, 10);
*/

INSERT INTO Restauration (ID_Restauration, date, montant, type, ID_oeuvre, raison_sociale) VALUES
(1, '2023-01-15', 20000.00, 'Nettoyage', 1, 'Bérengère BALLET-GOULARD'),
(2, '2023-02-20', 50000.00, 'Reparation', 2, 'Atelier Bouchet'),
(3, '2023-03-10', 15000.00, 'Conservation', 3, 'Cadre Noir'),
(4, '2023-04-05', 30000.00, 'Restauration', 4, 'L ATELIER… tout court'),
(5, '2023-05-18', 45000.00, 'Renovation', 5, 'Cadre Noir'),
(6, '2023-07-30', 25000.00, 'Nettoyage', 6, 'Bérengère BALLET-GOULARD'),
(7, '2023-08-19', 35000.00, 'Reparation', 7, 'L ATELIER… tout court'),
(8, '2023-09-21', 50000.00, 'Restauration', 8, 'Atelier Bouchet'),
(9, '2023-10-14', 60000.00, 'Renovation', 9, 'L ATELIER… tout court'),
(10, '2023-11-05', 40000.00, 'Nettoyage', 10, 'Atelier Bouchet');

INSERT INTO Peinture (ID_oeuvre) VALUES
(1),
(3),
(4),
(6),
(7),
(9),
(10),
(11);

INSERT INTO Sculpture (ID_oeuvre) VALUES
(2),
(5);

INSERT INTO Photo (ID_oeuvre) VALUES
(8);

INSERT INTO Presente (ID_oeuvre, nom_exposition) VALUES
(7, 'Naples à Paris'),
(8, 'Photographie portrait');

/*
ANCIENNE TABLE INSERT SALLE
INSERT INTO Salle (numero, cap_max, exposition) VALUES
(40, 150,  'Photographie portrait'),
(41, 125,  'Photographie portrait'),
(42, 150, 'Photographie portrait'),
(67, 50, NULL),
(68, 70, NULL),
(69, 100, NULL),
(70, 70, NULL),
(101, 200, 'Naples à Paris'),
(102, 150, NULL),
(103, 250,  'Naples à Paris');
*/

INSERT INTO Salle (numero, cap_max, exposition,Panneau_Explicatif) VALUES
(40, 150,  'Photographie portrait','[
{"num":1,"texte":"Ancienne résidence de chasse des souverains Bourbon, le palais (la Reggia en italien) abrite aujourd’hui l’un des plus grands musées d Italie et l une des plus importantes pinacothèques d’Europe, tant par le nombre que par la qualité exceptionnelle des œuvres conservées.","salle" : 101}]'
),
(41, 125,  'Photographie portrait','[
{"num":2, "texte":"Une ambitieuse programmation culturelle donnera à cette invitation, au-delà des salles du musée, les dimensions d’une véritable saison napolitaine à Paris","salle":102}]'),
(42, 150, 'Photographie portrait','[{"num":3, "texte":"Ces images vont de ses débuts en tant que photographe - alors qu elle est étudiante au San Francisco Art Institute et utilise des pellicules noir et blanc - jusqu à celles en couleurs d’aujourd’hui.","salle":40}]'),

(67, 50, NULL,'[{"num":4, "texte":"La portraitiste succède à Raghu Rai, lauréat 2019 de ce jeune prix de consécration qui récompense tous les deux ans l ensemble de la carrière et l engagement de photographes.","salle":41}]'),

(68, 70, NULL,'[{"num":5, "texte":"La Joconde est représentée à mi-corps, bras et mains visibles, assise sur une chaise. Derrière elle, se dessine un sublime paysage qui est presque un tableau à part entière.","salle":67}]'),

(69, 100, NULL,'[{"num":6, "texte":"Le Radeau de la Méduse – initialement intitulé Scène d un naufrage – est l histoire d un scandale. Celui d une catastrophe maritime survenue en 1816, qui provoqua la mort de 160 personnes, noyées au large de la Mauritanie après avoir embarqué sur la frégate Méduse.","salle":68}]'),

(70, 70, NULL,'[{"num":7, "texte":"Elle est un hommage à la Grèce ancienne, le berceau de la démocratie, ainsi qu aux traditions de la république romaine. Cependant, Delacroix associe la liberté à des symboles modernes également, en lui faisant tenir un drapeau tricolore dans une main et une baïonnette dans la utre.","salle":69}]'),

(101, 200, 'Naples à Paris','[{"num":8, "texte":"L huile sur toile met en scène un récit tiré du Nouveau Testament. Dans la ville de Cana, en Galilée, Jésus, accompagné de sa mère Marie et ses disciples, est invité à un repas de noce.","salle":70}]'),

(102, 150, NULL,'[{"num":9, "texte":"Dominant la baie de Naples avec des vues splendides sur la mer, la ville et le Vésuve, le domaine de Capodimonte fut aménagé à partir de 1734 par la volonté du roi de Naples, Charles III de Bourbon.","salle":103}]'),

(103, 250,  'Naples à Paris','[{"num":10, "texte":"Psyché, la jeune fille d un roi était d une beauté sans égale. Elle était souvent comparée à Vénus, et la surpassait même.","salle":102}]');
/*
INSERT INTO Panneau_Explicatif (num, texte, salle) VALUES
(1, 'Ancienne résidence de chasse des souverains Bourbon, le palais (la Reggia en italien) abrite aujourd’hui l’un des plus grands musées d Italie et l une des plus importantes pinacothèques d’Europe, tant par le nombre que par la qualité exceptionnelle des œuvres conservées.', 101),
(2, 'Une ambitieuse programmation culturelle donnera à cette invitation, au-delà des salles du musée, les dimensions d’une véritable saison napolitaine à Paris', 102),
(3, 'Ces images vont de ses débuts en tant que photographe - alors qu elle est étudiante au San Francisco Art Institute et utilise des pellicules noir et blanc - jusqu à celles en couleurs d’aujourd’hui.', 40),
(4, 'La portraitiste succède à Raghu Rai, lauréat 2019 de ce jeune prix de consécration qui récompense tous les deux ans l ensemble de la carrière et l engagement de photographes.', 41),
(5, 'La Joconde est représentée à mi-corps, bras et mains visibles, assise sur une chaise. Derrière elle, se dessine un sublime paysage qui est presque un tableau à part entière.',67),
(6, ' Le Radeau de la Méduse – initialement intitulé Scène d un naufrage – est l histoire d un scandale. Celui d une catastrophe maritime survenue en 1816, qui provoqua la mort de 160 personnes, noyées au large de la Mauritanie après avoir embarqué sur la frégate Méduse.', 68),
(7,'Elle est un hommage à la Grèce ancienne, le berceau de la démocratie, ainsi qu aux traditions de la république romaine. Cependant, Delacroix associe la liberté à des symboles modernes également, en lui faisant tenir un drapeau tricolore dans une main et une baïonnette dans la utre.', 69),
(8, 'L huile sur toile met en scène un récit tiré du Nouveau Testament. Dans la ville de Cana, en Galilée, Jésus, accompagné de sa mère Marie et ses disciples, est invité à un repas de noce.', 70),
(9, 'Dominant la baie de Naples avec des vues splendides sur la mer, la ville et le Vésuve, le domaine de Capodimonte fut aménagé à partir de 1734 par la volonté du roi de Naples, Charles III de Bourbon.',103),
(10,'Psyché, la jeune fille d un roi était d une beauté sans égale. Elle était souvent comparée à Vénus, et la surpassait même.',102);
*/
/*
INSERT INTO Creneau (jour, h_debut) VALUES
('Lundi', 10),
('Mardi', 14),
('Mercredi', 9),
('Jeudi', 12),
('Vendredi', 11),
('Mercredi', 13),
('Lundi',14),
('Jeudi', 9),
('Vendredi', 17),
('Lundi', 15);
*/
/*
INSERT INTO Guide (id, nom, prenom, adresse, date_embauche) VALUES
(1, 'Durand', 'Julie', '45 Allee des Tulipes, 75014 Paris', '2021-05-08'),
(2, 'Guillot', 'Emilie', '29 Boulevard des Fleurs, 75016 Paris', '2019-09-15'),
(3, 'Lefevre', 'Léo', '32 Avenue de la Republique, 75008 Paris', '2022-03-21'),
(4, 'Garnier', 'Lucie', '8 Place de la Victoire, 75003 Paris', '2018-11-30'),
(5, 'Leroy', 'Sarah', '17 Chemin des Vignes, 75008 Paris', '2021-07-02'),
(6, 'Roux', 'Louis', '26 Rue de la Gare, 75004 Paris', '2020-04-12'),
(7, 'Perez', 'Celine', '36 Cours de l Europe, 75002Paris', '2023-01-15'),
(8, 'Moreau', 'Adam', '41 Rue des Ecoles, 75020 Paris', '2018-07-20'),
(9, 'Huet', 'Gabriel', '25 Avenue du Maine, 75015 Paris', '2022-09-01'),
(10, 'Fleury', 'Raphael', '63 Boulevard Lefort, 75006 Paris', '2021-06-18');
*/
/*
INSERT INTO Affecte (jour, h_debut, guide) VALUES
('Lundi', 10, 1),
('Mardi', 14, 2),
('Mercredi', 9, 3),
('Jeudi', 12, 3),
('Vendredi', 11, 4),
('Mercredi', 13, 5),
('Lundi',14, 6),
('Jeudi', 9, 7),
('Vendredi', 17, 8),
('Lundi', 15, 2);
*/
/*
INSERT INTO Mene_exp_temp (id, nom) VALUES
(1, 'Impressionnisme'),
(5, 'Renaissance Italienne'),
(8, 'Modernistes'),
(2, 'Le trésor de Notre Dame de Paris'),
(9, 'Naples à Paris'),
(4, 'Photographie portrait');
*/
/*
INSERT INTO Mene_exp_perm (id, nom) VALUES
(2, 'Classiques du 20e siecle'),
(3, 'Art du Moyen Age'),
(6, 'Cubisme'),
(7, 'Exposition surréaliste');
*/

/*
Bibliogrpahie utilisée pour les textes des panneaux explicatifs:

  1. BUITEKANT, Esther, 2020. La Joconde : mystères et secrets d’un chef-d’oeuvre. Geo.fr. [en ligne]. 31 août 2020. Disponible à l’adresse: https://www.geo.fr/histoire/la-joconde-mysteres-et-secrets-dun-chef-doeuvre-201892

  2. Le Radeau de la Méduse, portrait d’un chef-d’œuvre, [sans date]. Beaux Arts. [en ligne]. Disponible à l’adresse: https://www.beauxarts.com/expos/le-radeau-de-la-meduse-portrait-dun-chef-doeuvre

  3. Les Noces de Cana : tableau de Véronèse au musée Louvre - PARISCityVISION, [sans date]. [en ligne]. Disponible à l’adresse: https://www.pariscityvision.com/fr/paris/musees/musee-du-louvre/noces-cana-veronese#:~:text=L%27huile%20sur%20toile%20met,J%C3%A9sus%20accomplit%20son%20premier%20miracle

  4. Naples à Paris - Le Louvre invite le musée de Capodimonte, [sans date]. Le Louvre. [en ligne]. Disponible à l’adresse: https://www.louvre.fr/en-ce-moment/expositions/naples-a-paris

  5. TRANCART, François-Xavier, 2019. La Liberté guidant le peuple d’Eugene Delacroix. Magazine Artsper. [en ligne]. 10 avril 2019. Disponible à l’adresse: https://blog.artsper.com/fr/la-minute-arty/analyse-dun-chef-doeuvre-la-liberte-guidant-le-peuple-deugene-delacroix

  6. ZOOM SUR : Psyché ranimée par le baiser de l’Amour, Antonio Canova (1787-1793), 2019. Museum TV. [en ligne]. Disponible à l’adresse: https://www.museumtv.art/artnews/oeuvres/zoom-sur-psyche-ranimee-par-le-baiser-de-lamour-dantonio-canova

  7. GROSSET, Justine, 2021. Exposition photo Annie Leibovitz, célébrée par l’Académie des beaux-arts. Phototrend.fr. [en ligne]. 5 novembre 2021. Disponible à l’adresse: https://phototrend.fr/2021/11/expo-photo-annie-leibovitz-academie-beaux-arts-paris
*/
